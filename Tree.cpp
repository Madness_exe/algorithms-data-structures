// AtCoder.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <stdio.h>
#include <algorithm>
#include <vector>
#include <set>
#include <string>
#include <map>
#include <math.h>
#include <cmath>

using namespace std;

class Node{
    public:
    int val;
    Node* left = NULL;
    Node* right = NULL;

    Node(int val){
        this->val = val;
    }
};

class Tree{
    public:
    Node* head;
    int lvl = 0;

    Tree(Node* head){
        this->head = head;
    }

    Node* insert(Node* n, int x){
        if(n == NULL){
            Node* node = new Node(x);
            return node;
        }
        else if(x < n->val) n->left = insert(n->left, x);
        else if(x > n->val) n->right = insert(n->right, x);
        return n;
    }

    void inOrder(Node* n){
        if(n == NULL) return;
        inOrder(n->left);
        cout << n->val << " ";
        inOrder(n->right);
    }

    void level(Node* n, int x){
        if(n == NULL) return;
        x++;
        lvl = max(lvl, x);
        level(n->left, x);
        level(n->right, x);
    }

};

int main() {
    int arr[7] = {8,5,9,7,10,2,3};
    Node* h = new Node(arr[0]);
    Tree* tree = new Tree(h);
    for(int i: arr){
        tree->insert(tree->head, i);
    }
    tree->inOrder(tree->head);
    cout << endl;
    tree->level(tree->head, 0);
    cout << tree->lvl << endl;
    return 0;
}